import csv
import pandas as pd
import pyodbc
import os

# connect to the MDB database

#if the file has passward use these commented line instead of (14,15 )lines 
#MDB = 'c:/path/to/my.mdb'
#DRV = '{Microsoft Access Driver (*.mdb)}'
#PWD = 'mypassword'
#conn = pyodbc.connect('DRIVER=%s;DBQ=%s;PWD=%s' % (DRV,MDB,PWD))

conn_string = r'DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=path to file .mdb;'
conn = pyodbc.connect(conn_string)
cursor = conn.cursor()
#through the cursor get all the table names and save then in a list
cursor.tables(tableType="TABLE")
tables = []
for table_info in cursor:
    tables.append(table_info.table_name)

# convert mdb to csv by looping through the list of tables and save them in csv files
for table_info in tables:
    with open(table_info + '.csv', 'w', newline='') as f_output:
        csv_output = csv.writer(f_output)
        cursor.execute("SELECT * FROM " + table_info)
        csv_output.writerow([i[0] for i in cursor.description])  # write headers
        csv_output.writerows(cursor.fetchall())  # write data



