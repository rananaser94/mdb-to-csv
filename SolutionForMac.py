import pandas_access as mdb
# pandas_access is sub package from subprocess
#simple code 
db_filename = 'Database2.mdb'

# Listing the tables.
for tbl in mdb.list_tables(db_filename):
  print(tbl)
  mdb.read_table(db_filename, tbl).to_csv(tbl + '.csv', index=False)
