import pandas as pd
import subprocess


#on MAC OS, their is no access to MS Access, and pandas package does n't work on MAC Mdb files 
#so their is subprocess package or its sub package pandas_access


#this function will return the list of tables in the mdb file
def show_tables(path):
    tables = subprocess.check_output(["mdb-tables", path])
    return tables.decode().split()

#this function will return the data in a specific table
def show_data(path, table):
    tables = subprocess.check_output(["mdb-export", path, table])
    return tables.decode().split('\n')

#convert the data to csv file and save the file
def convert_df(path, table):
    d = show_data(path, table)
    columns = d[0].split(',')
    data = [i.split(',') for i in d[1:]]
    df = pd.DataFrame(columns=columns, data=data)
    return df


table = show_tables("fileName.mdb")
print(table)
#convert mdb to csv
for i in table:
    df = convert_df("Database2.mdb", i)
    df.to_csv(i + '.csv', index=False, )
